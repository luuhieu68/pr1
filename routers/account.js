const { json } = require('body-parser')
const express = require('express')
const router = express.Router()
const accountModel = require('../models/account')

const PAGE_SIZE = 2

//Phan  trang
router.get('/user', (req, res, next) => {
    var page = req.query.page
    if (page) {
        page = parseInt(page)
        if(page <1){
            page = 1
        }

        var boQua = (page -1) * PAGE_SIZE

        accountModel.find({})
        .skip(boQua)
        .limit(PAGE_SIZE)
        .then(data => {
            res.json(data)
        })
        .catch(err => {
            res.status(500).json('server loi')
        })

    }
    else {

        accountModel.find({})
            .then(data => {
                res.json(data)
            })
            .catch(err => {
                res.status(500).json('server loi')
            })
    }

})


//lay du lieu
router.get('/', (req, res, next) => {
    accountModel.find({})
        .then(data => {
            res.json(data)
        })
        .catch(err => {
            res.status(500).json('server loi')
        })
})

//lay du lieu 1 phan tu theo id
router.get('/:id', (req, res, next) => {
    var id = req.params.id
    accountModel.findById(id)
        .then(data => {
            res.json(data)
        })
        .catch(err => {
            res.status(500).json('server loi')
        })
})



//them moi du lieu
router.post('/', (req, res, next) => {
    var user = req.body.username
    var pass = req.body.password
    accountModel.findOne({
        username: user
    })
        .then(data => {
            if (data) {
                res.json('User da ton tai')
            }
            else {
                return accountModel.create({
                    username: user,
                    password: pass
                })
            }
        })
        .then(data => {
            res.json('Tao thanh cong')
        })
        .catch(err => {
            res.status(500).json('that bai')
        })
})

//update du lieu
router.put('/:id', (req, res, next) => {
    var id = req.params.id
    var newPass = req.body.newPass
    accountModel.findByIdAndUpdate(id, {
        password: newPass
    })
        .then(data => {
            res.json('Thanh cong')
        })
        .catch(err => {
            res.status(500).json('Loi server')
        })
})

//xoa du lieu
router.delete('/:id', (req, res, next) => {
    var id = req.params.id
    accountModel.deleteOne({
        _id: id
    })
        .then(data => {
            res.json('Xoa thanh cong')
        })
        .catch(err => {
            res.status(500).json('loi')
        })
})





module.exports = router