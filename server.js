const express = require('express')
const bodyParser = require('body-parser')
const accountModel = require('./models/account')
const accountRouter = require('./routers/account')



const app = express()
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

//Dang ky tai khoan
app.post('/register', (req, res, next) =>{
    var user = req.body.username
    var pass = req.body.password

    accountModel.findOne({
        username: user
    })
    .then(data =>{
        if(data){
            res.json('User da ton tai')
        }
        else{
            return accountModel.create({
                username: user,
                password: pass
            })
        }
    })
    .then(data =>{
        res.json('Tao thanh cong')
    })
    .catch(err =>{
        res.status(500).json('that bai')
    })
})

//Dang nhap
app.post('/login', (req, res, next) =>{
    var user = req.body.username
    var pass = req.body.password
    accountModel.findOne({
        username: user,
        password: pass
    })
    .then(data =>{
        if(data){
            res.json('Dang nhap thanh cong')
        }
        else{
            res.status(400).json('Tai khoan khong ton tai')
        }
    })
    .catch(err =>{
        res.status(500).json('Sever bi loi')
    })
})


app.use('/api/account/', accountRouter)

app.get('/', (req, res, next) =>{
    res.send('hihi')
})

app.listen(3000, () =>  {
    console.log('ahihi')
})