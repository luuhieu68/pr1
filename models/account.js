const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost:27017/data2', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})

const Schema = mongoose.Schema
const AccountSchema = new Schema({
  username: String,
  password: String
},{
    collection: 'account'
})

const AccountModel = mongoose.model('account', AccountSchema)
module.exports = AccountModel

//Tao account nhanh
// for(let i =0; i<20; i++){
//     AccountModel.create({
//         username: 'admin' + i,
//         password: '123456'
//     })
// }